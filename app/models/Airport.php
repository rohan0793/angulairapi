<?php

class Airport extends \Eloquent {
	protected $fillable = [];

	public function flights_from(){
		return $this->hasMany('Flight', 'origin');
	}

	public function flights_to(){
		return $this->hasMany('Flight', 'destination');
	}
}