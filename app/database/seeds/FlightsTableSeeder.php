<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class FlightsTableSeeder extends Seeder {

	public function run()
	{
		$flights = json_decode(file_get_contents('public/flights.json'), true);

		foreach ($flights as $flight) {
			$flight['origin'] = Airport::where('code', '=', $flight['origin'])->first()->id;
			$flight['destination'] = Airport::where('code', '=', $flight['destination'])->first()->id;
			Flight::create($flight);
		}
	}

}