<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flights', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('number');
			$table->integer('origin')->unsigned();
			$table->foreign('origin')->references('id')->on('airports');
			$table->integer('destination')->unsigned();
			$table->foreign('destination')->references('id')->on('airports');
			$table->integer('price');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flights');
	}

}
