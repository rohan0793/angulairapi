<?php

class Reservation extends \Eloquent {
	protected $fillable = ['user_id', 'flight_id'];

	public function flight(){
		return $this->belongsTo('Flight');
	}

	public function user(){
		return $this->belongsTo('User');
	}
}