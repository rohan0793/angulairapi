<?php

class AuthToken extends \Eloquent {
	protected $fillable = ['user_id', 'auth_token'];

	public function user(){
		return $this->belongsTo('User');
	}

	public static function logged_in_user_id($auth_token){
		return AuthToken::where('auth_token', '=', $auth_token)->first()->user_id;
	}
}