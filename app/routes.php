<?php

Route::resource('users', 'UserController');

Route::resource('airports', 'AirportController');

Route::get('/flights/search', 'FlightController@search');
Route::resource('flights', 'FlightController');

Route::get('reservations/search', 'ReservationController@search');
Route::resource('reservations', 'ReservationController');

Route::get('auth', 'AuthController@index');
Route::post('auth', 'AuthController@store');
Route::delete('auth', 'AuthController@destroy');