<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AirportsTableSeeder extends Seeder {

	public function run()
	{
		$airports = json_decode(file_get_contents('public/airports.json'), true);

		foreach ($airports as $airport) {
			array_pop($airport);
			Airport::create($airport);
		}
	}

}