<?php

class AuthController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$auth_token = Input::get('auth_token');
		$auth_token = Request::header('Authorization');

		if(!AuthToken::where('auth_token', '=', $auth_token)->first()){
			return Response::json([], 401);
		}

		$user = AuthToken::where('auth_token', '=', $auth_token)->first()->user;

		return Response::json($user, 200);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($credentials = Input::all(), [
			'email' => 'required|email',
			'password' => 'required'
		]);

		if($validator->fails()){
			return Response::json($validator->messages(), 401);
		}

		unset($credentials['auth_token']);

		if(!Auth::validate($credentials)){
			return Response::json([], 401);
		}

		$user = User::where('email', '=', $credentials['email'])->first();
		$auth_token = str_random(40);

		AuthToken::create([
			'user_id' => $user->id,
			'auth_token' => $auth_token
		]);

		return Response::json([
			'auth_token' => $auth_token
		], 200);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		$auth_token = Request::header('Authorization');
		//$auth_token = Input::get('auth_token');

		if(!AuthToken::where('auth_token', '=', $auth_token)->first()){
			return Response::json([], 401);
		}

		AuthToken::where('auth_token', '=', $auth_token)->first()->delete();

		return Response::json([], 204);
	}


}
