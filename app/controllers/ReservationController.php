<?php

class ReservationController extends \BaseController {

	public function __construct(){
		$this->beforeFilter('auth_token');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$reservaions = Reservation::with('flight.origin', 'flight.destination')
							->where('user_id', '=', AuthToken::logged_in_user_id(Request::header('Authorization')))
							->get();

		return Response::json($reservaions);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), [
			'user_id' => 'required|exists:users,id',
			'flight_id' => 'required|exists:flights,id'
		]);

		if($validator->fails()){
			return Response::make($validator->messages(), 400);
		}

		if($data['user_id'] != AuthToken::logged_in_user_id(Request::header('Authorization'))){
			return Response::json([], 401);
		}

		if(Reservation::where('user_id', '=', $data['user_id'])->where('flight_id', '=', $data['flight_id'])->first()){
			return Response::json(['flight_id' => ['This flight is already reserved.']], 400);
		}

		$reservation = Reservation::create(Input::all());

		$reservation = Reservation::with('flight.origin', 'flight.destination')->find($reservation->id);
		return Response::json($reservation, 200);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$reservation = Reservation::with('flight.origin', 'flight.destination')
			->where('user_id', '=', AuthToken::logged_in_user_id(Request::header('Authorization')))
			->findOrFail($id);

		return Response::json($reservation, 200);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$reservation = Reservation::find($id);
		if($reservation->user->id != AuthToken::logged_in_user_id(Request::header('Authorization'))){
			return Response::json([], 401);
		}

		$reservation->delete();

		return Response::json([], 204);
	}


}
