<?php

class AirportController extends \BaseController {

	public function __construct(){
		$this->beforeFilter('auth_token');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$airports = Airport::with('flights_from.destination', 'flights_to.origin')->get();
		return Response::json($airports, 200);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(is_string($id)){
			$airport = Airport::with('flights_from.destination', 'flights_to.origin')->where('code', '=', $id)->first();
		}

		if(is_numeric($id)){
			$airport = Airport::with('flights_from.destination', 'flights_to.origin')->findorFail($id);
		}
		
		return Response::json($airport, 200);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
