<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		// Creating a fake admin user
		User::create([
			'email' => 'admin@admin.com',
			'password' => Hash::make('admin12345')
		]);

		/*
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			User::create([

			]);
		}
		*/
	}

}