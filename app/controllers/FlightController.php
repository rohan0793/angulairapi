<?php

class FlightController extends \BaseController {

	public function __construct(){
		$this->beforeFilter('auth_token');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$flights = Flight::with('origin', 'destination')->get();
		return Response::json($flights, 200);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($number)
	{
		$flight = Flight::with('origin', 'destination')->where('number', '=', $number)->first();

		return Response::json($flight, 200);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function search(){

		// Fetching all flights with all the origins and destinations
		$flights = Flight::with('origin', 'destination');

		$origin = Input::get('origin');
		$destination = Input::get('destination');

		// If the URL params have code variable set to true,
		// search will be according to the 'code' field of
		// destinations and origins otherwise 'name' field
		if(!Input::get('code')){
			$flights = $flights->whereHas('origin', function($q) use ($origin){
				$q->where('name', 'LIKE', "%$origin%");
			})->whereHas('destination', function($q) use ($destination){
				$q->where('name', 'LIKE', "%$destination%");
			})->get();
		}else{
			$flights = $flights->whereHas('origin', function($q) use ($origin){
				$q->where('code', 'LIKE', "%$origin%");
			})->whereHas('destination', function($q) use ($destination){
				$q->where('code', 'LIKE', "%$destination%");
			})->get();
		}

		return Response::json($flights, 200);
	}


}
