<?php

class Flight extends \Eloquent {
	protected $fillable = ['number', 'origin', 'destination'];

	public function origin(){
		return $this->belongsTo('Airport', 'origin');
	}

	public function destination(){
		return $this->belongsTo('Airport', 'destination');
	}
}